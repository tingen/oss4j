package com.bgi.cg.api;

import com.bgi.cg.oss4j.common.factory.BeanFactory;

import java.util.concurrent.Executor;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public abstract class AbstractOssService implements BaseOssService {
    protected final Executor pool;

    protected AbstractOssService(Executor pool) {
        this.pool = pool;
    }

    protected AbstractOssService() {
        this.pool = BeanFactory.getExecutor();
    }
}
