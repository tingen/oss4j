package com.bgi.cg.api.entity;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.io.InputStream;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Data
@Builder
public class OssResponse {
    private String filePath;
    public String previewUrl;
    public String externalUrl;
    public String fileDomain;
    public String ossType;
    public String code;
    public String errMsg;
    public InputStream content;

    public boolean isOk() {
        return code.equals("0");
    }

    public void setOk() {
        this.code = "0";
    }

    public OssResponse setFail(String... errMsg) {
        this.code = "-1";
        if (errMsg.length > 0) {
            this.errMsg = errMsg[0];
        }
        return this;
    }
}
