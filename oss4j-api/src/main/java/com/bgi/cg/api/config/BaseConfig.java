package com.bgi.cg.api.config;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @Description 云厂商基础配置
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Data
@SuperBuilder
@NoArgsConstructor
public class BaseConfig {

    private String accessKey;
    private String accessKeySecret;
    private String bucket;
    /**
     * 文件上传域名
     **/
    private String endPoint;
    /**
     * 当桶不存在时，是否自动创建桶
     */
    @Builder.Default
    private boolean bucketAutoCreate = true;
}
