package com.bgi.cg.api;

import com.bgi.cg.api.entity.OssResponse;

import java.io.File;
import java.util.function.Function;

/**
 * @author Z.cg
 **/
public interface BaseOssService {

    /**
     * <p> 文件上传，使用默认配置上传文件
     *
     * @param file 需要上传的文件
     * @return
     */
    OssResponse upload(File file);

    /**
     * <p> 文件上传，使用默认配置上传文件，指定桶的名称
     *
     * @param file       需要上传的文件
     * @param bucketName 自定义上传桶，自定义桶名，其他配置按照yml中的配置
     * @return
     */
    OssResponse upload(File file, String bucketName);

    /**
     * <p> 文件上传，使用默认配置上传文件，指定文件名称生成的逻辑
     *
     * @param file            需要上传的文件
     * @param fileKeyFunction 自定义上传文件名称，如果想要通过目录区分，名称中使用/拼接
     * @return
     */
    OssResponse upload(File file, Function<File, String> fileKeyFunction);

    /**
     * <p> 文件上传，使用默认配置上传文件，指定桶的名称，指定文件名称生成的逻辑
     *
     * @param file            需要上传的文件
     * @param bucketName      自定义桶名称，自定义桶名，其他配置按照yml中的配置
     * @param fileKeyFunction 自定义上传文件名称，如果想要通过目录区分，名称中使用/拼接
     * @return
     */
    OssResponse upload(File file, String bucketName, Function<File, String> fileKeyFunction);

    /**
     * <p> 获取文件预览地址，然后使用预览地址文件下载
     *
     * @param fileName 文件名称(文件全路径)
     * @return
     */
    OssResponse genPreviewUrl(String fileName);

    /**
     * <p> 获取文件预览地址，然后使用预览地址文件下载，并指定桶名称
     *
     * @param fileName   文件名称(文件全路径)
     * @param bucketName 自定义桶名称，自定义桶名，其他配置按照yml中的配置
     * @return
     */
    OssResponse genPreviewUrl(String fileName, String bucketName);

    /**
     * <p> 删除文件，使用默认配置的桶
     *
     * @param fileName 文件名称(文件全路径)
     * @return
     */
    OssResponse delete(String fileName);

    /**
     * <p> 删除指定桶中的文件
     *
     * @param fileName   文件名称(文件全路径)
     * @param bucketName
     * @return
     */
    OssResponse delete(String fileName, String bucketName);

}
