package com.bgi.cg.oss4j.provider._base;

import com.bgi.cg.api.BaseOssService;
import com.bgi.cg.api.config.OssSupplierConfig;

public interface BaseOssProviderFactory<S extends BaseOssService, C extends OssSupplierConfig> {
    S createOss();

    S createMultiOss(C c);

    C getConfig();

    S refresh(C config);
}
