package com.bgi.cg.oss4j.provider.upyun.factory;

import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider.upyun.config.UpyunOssConfig;
import com.bgi.cg.oss4j.provider.upyun.service.UpyunOssService;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public class UpyunOssFactory implements BaseOssProviderFactory<UpyunOssService, UpyunOssConfig> {
    private static UpyunOssService ossService;
    private static final UpyunOssFactory INSTANCE = new UpyunOssFactory();

    private static final class ConfigHolder {
        private static UpyunOssConfig config = UpyunOssConfig.builder().build();
    }

    @Override
    public UpyunOssService createOss() {
        if (ossService == null) {
            ossService = createMultiOss(getConfig());
        }
        return ossService;
    }

    @Override
    public UpyunOssService createMultiOss(UpyunOssConfig config) {
        return new UpyunOssService(config, BeanFactory.getExecutor());
    }

    @Override
    public UpyunOssConfig getConfig() {
        return ConfigHolder.config;
    }

    @Override
    public UpyunOssService refresh(UpyunOssConfig config) {
        setConfig(config);
        ossService = createMultiOss(getConfig());
        return ossService;
    }

    void setConfig(UpyunOssConfig config) {
        ConfigHolder.config = config;
    }

    public static UpyunOssFactory instance() {
        return INSTANCE;
    }
}
