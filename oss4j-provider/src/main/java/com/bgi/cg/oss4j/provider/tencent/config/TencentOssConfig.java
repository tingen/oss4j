package com.bgi.cg.oss4j.provider.tencent.config;

import cn.hutool.core.util.StrUtil;
import com.bgi.cg.api.config.BaseConfig;
import com.bgi.cg.api.config.OssSupplierConfig;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Data
@SuperBuilder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class TencentOssConfig extends BaseConfig implements OssSupplierConfig {
    // 桶所在的区域
    private String region;
    /**
     * 允许访问前缀，默认所有，风险较高
     */
    @Builder.Default
    private String allowPreFixes = "*";
    /**
     * 密钥的权限列表
     * name/cos:PutObject 表示简单上传
     * 其他：
     * //表单上传、小程序上传
     * "name/cos:PostObject",
     * // 分块上传
     * "name/cos:InitiateMultipartUpload",
     * "name/cos:ListMultipartUploads",
     * "name/cos:ListParts",
     * "name/cos:UploadPart",
     * "name/cos:CompleteMultipartUpload"
     */
    @Builder.Default
    private String allowActions = "name/cos:PutObject";

    @Override
    public boolean configIsOk() {
        return StrUtil.isAllNotBlank(this.getAccessKey(), this.getAccessKeySecret(), this.getBucket(), this.getEndPoint());
    }
}
