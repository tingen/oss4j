package com.bgi.cg.oss4j.provider.minio.factory;

import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider.huawei.config.HuaweiOssConfig;
import com.bgi.cg.oss4j.provider.huawei.factory.HuaweiOssFactory;
import com.bgi.cg.oss4j.provider.huawei.service.HuaweiOssService;
import com.bgi.cg.oss4j.provider.minio.config.MinioOssConfig;
import com.bgi.cg.oss4j.provider.minio.service.MinioOssService;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public class MinioOssFactory implements BaseOssProviderFactory<MinioOssService, MinioOssConfig> {
    private static MinioOssService ossService;
    private static final MinioOssFactory INSTANCE = new MinioOssFactory();

    private static final class ConfigHolder {
        private static MinioOssConfig config = MinioOssConfig.builder().build();
    }

    @Override
    public MinioOssService createOss() {
        if (ossService == null) {
            ossService = createMultiOss(getConfig());
        }
        return ossService;
    }

    @Override
    public MinioOssService createMultiOss(MinioOssConfig config) {
        return new MinioOssService(config, BeanFactory.getExecutor());
    }

    @Override
    public MinioOssConfig getConfig() {
        return ConfigHolder.config;
    }

    @Override
    public MinioOssService refresh(MinioOssConfig config) {
        setConfig(config);
        ossService = createMultiOss(getConfig());
        return ossService;
    }

    void setConfig(MinioOssConfig config) {
        ConfigHolder.config = config;
    }

    public static MinioOssFactory instance() {
        return INSTANCE;
    }
}
