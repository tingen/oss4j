package com.bgi.cg.oss4j.provider.minio.config;

import cn.hutool.core.util.StrUtil;
import com.bgi.cg.api.config.BaseConfig;
import com.bgi.cg.api.config.OssSupplierConfig;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Data
@SuperBuilder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class MinioOssConfig extends BaseConfig implements OssSupplierConfig {
    @Override
    public boolean configIsOk() {
        return StrUtil.isAllNotBlank(this.getAccessKey(), this.getAccessKeySecret(), this.getBucket(), this.getEndPoint());
    }
}
