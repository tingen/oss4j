package com.bgi.cg.oss4j.provider.huawei.config;

import cn.hutool.core.util.StrUtil;
import com.bgi.cg.api.config.BaseConfig;
import com.bgi.cg.api.config.OssSupplierConfig;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Data
@SuperBuilder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class HuaweiOssConfig extends BaseConfig implements OssSupplierConfig {
    // 查看region与endpoint https://developer.huaweicloud.com/endpoint?OBS
    private String region;

    @Override
    public boolean configIsOk() {
        return StrUtil.isAllNotBlank(this.getAccessKey(), this.getAccessKeySecret()
                , this.getBucket(), this.getEndPoint(), this.getRegion());
    }
}
