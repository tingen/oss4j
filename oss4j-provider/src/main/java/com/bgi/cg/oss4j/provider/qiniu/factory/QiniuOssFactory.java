package com.bgi.cg.oss4j.provider.qiniu.factory;

import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider.aliyun.config.AliyunOssConfig;
import com.bgi.cg.oss4j.provider.aliyun.service.AliyunOssService;
import com.bgi.cg.oss4j.provider.minio.config.MinioOssConfig;
import com.bgi.cg.oss4j.provider.minio.factory.MinioOssFactory;
import com.bgi.cg.oss4j.provider.minio.service.MinioOssService;
import com.bgi.cg.oss4j.provider.qiniu.config.QiniuOssConfig;
import com.bgi.cg.oss4j.provider.qiniu.service.QiniuOssService;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public class QiniuOssFactory implements BaseOssProviderFactory<QiniuOssService, QiniuOssConfig> {
    private static QiniuOssService ossService;
    private static final QiniuOssFactory INSTANCE = new QiniuOssFactory();

    private static final class ConfigHolder {
        private static QiniuOssConfig config = QiniuOssConfig.builder().build();
    }

    @Override
    public QiniuOssService createOss() {
        if (ossService == null) {
            ossService = createMultiOss(getConfig());
        }
        return ossService;
    }

    @Override
    public QiniuOssService createMultiOss(QiniuOssConfig config) {
        return new QiniuOssService(config, BeanFactory.getExecutor());
    }

    @Override
    public QiniuOssConfig getConfig() {
        return ConfigHolder.config;
    }

    @Override
    public QiniuOssService refresh(QiniuOssConfig config) {
        setConfig(config);
        ossService = createMultiOss(config);
        return ossService;
    }

    void setConfig(QiniuOssConfig config) {
        ConfigHolder.config = config;
    }

    public static QiniuOssFactory instance() {
        return INSTANCE;
    }
}
