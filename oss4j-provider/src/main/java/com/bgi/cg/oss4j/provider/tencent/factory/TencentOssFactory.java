package com.bgi.cg.oss4j.provider.tencent.factory;

import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider.tencent.config.TencentOssConfig;
import com.bgi.cg.oss4j.provider.tencent.service.TencentOssService;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public class TencentOssFactory implements BaseOssProviderFactory<TencentOssService, TencentOssConfig> {
    private static TencentOssService ossService;
    private static final TencentOssFactory INSTANCE = new TencentOssFactory();

    private static final class ConfigHolder {
        private static TencentOssConfig config = TencentOssConfig.builder().build();
    }

    @Override
    public TencentOssService createOss() {
        if (ossService == null) {
            ossService = createMultiOss(getConfig());
        }
        return ossService;
    }

    @Override
    public TencentOssService createMultiOss(TencentOssConfig config) {
        return new TencentOssService(config, BeanFactory.getExecutor());
    }

    @Override
    public TencentOssConfig getConfig() {
        return ConfigHolder.config;
    }

    @Override
    public TencentOssService refresh(TencentOssConfig tencentOssConfig) {
        setConfig(tencentOssConfig);
        ossService = createMultiOss(getConfig());
        return ossService;
    }

    void setConfig(TencentOssConfig config) {
        ConfigHolder.config = config;
    }

    public static TencentOssFactory instance() {
        return INSTANCE;
    }
}
