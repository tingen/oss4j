package com.bgi.cg.oss4j.provider._base;

import com.bgi.cg.api.AbstractOssService;
import com.bgi.cg.api.config.OssSupplierConfig;
import com.bgi.cg.oss4j.provider.aliyun.factory.AliyunOssFactory;
import com.bgi.cg.oss4j.provider.huawei.factory.HuaweiOssFactory;
import com.bgi.cg.oss4j.provider.minio.factory.MinioOssFactory;
import com.bgi.cg.oss4j.provider.qiniu.factory.QiniuOssFactory;
import com.bgi.cg.oss4j.provider.tencent.factory.TencentOssFactory;
import com.bgi.cg.oss4j.provider.upyun.factory.UpyunOssFactory;

public enum OssSupplierType {
    ALI("阿里云", AliyunOssFactory.instance()),
    QINIU("七牛云", QiniuOssFactory.instance()),
    TENCENT("腾讯云", TencentOssFactory.instance()),
    HUAWEI("华为云", HuaweiOssFactory.instance()),
    UPYUN("又拍云", UpyunOssFactory.instance()),
    MINIO("MINIO", MinioOssFactory.instance());
    private final String name;
    /**
     * 短信对象配置
     */
    private final BaseOssProviderFactory<? extends AbstractOssService, ? extends OssSupplierConfig> ossProviderFactory;


    OssSupplierType(String name, BaseOssProviderFactory<? extends AbstractOssService, ? extends OssSupplierConfig> ossProviderFactory) {
        this.name = name;
        this.ossProviderFactory = ossProviderFactory;
    }

    public String getName() {
        return name;
    }

    public BaseOssProviderFactory<? extends AbstractOssService, ? extends OssSupplierConfig> getOssProviderFactory() {
        return ossProviderFactory;
    }
}
