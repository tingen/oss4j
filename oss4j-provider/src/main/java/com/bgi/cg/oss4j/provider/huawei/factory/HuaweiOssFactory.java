package com.bgi.cg.oss4j.provider.huawei.factory;

import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider.huawei.config.HuaweiOssConfig;
import com.bgi.cg.oss4j.provider.huawei.service.HuaweiOssService;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public class HuaweiOssFactory implements BaseOssProviderFactory<HuaweiOssService, HuaweiOssConfig> {
    private static HuaweiOssService ossService;
    private static final HuaweiOssFactory INSTANCE = new HuaweiOssFactory();

    private static final class ConfigHolder {
        private static HuaweiOssConfig config = HuaweiOssConfig.builder().build();
    }

    @Override
    public HuaweiOssService createOss() {
        if (ossService == null) {
            ossService = createMultiOss(getConfig());
        }
        return ossService;
    }

    @Override
    public HuaweiOssService createMultiOss(HuaweiOssConfig config) {
        return new HuaweiOssService(config, BeanFactory.getExecutor());
    }

    @Override
    public HuaweiOssConfig getConfig() {
        return ConfigHolder.config;
    }

    @Override
    public HuaweiOssService refresh(HuaweiOssConfig config) {
        setConfig(config);
        ossService = createMultiOss(getConfig());
        return ossService;
    }

    void setConfig(HuaweiOssConfig config) {
        ConfigHolder.config = config;
    }

    public static HuaweiOssFactory instance() {
        return INSTANCE;
    }
}
