package com.bgi.cg.oss4j.provider.aliyun.factory;

import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider.aliyun.config.AliyunOssConfig;
import com.bgi.cg.oss4j.provider.aliyun.service.AliyunOssService;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
public class AliyunOssFactory implements BaseOssProviderFactory<AliyunOssService, AliyunOssConfig> {
    private static AliyunOssService ossService;
    private static final AliyunOssFactory INSTANCE = new AliyunOssFactory();

    private static final class ConfigHolder {
        private static AliyunOssConfig config = AliyunOssConfig.builder().build();
    }

    @Override
    public AliyunOssService createOss() {
        if (ossService == null) {
            ossService = createMultiOss(getConfig());
        }
        return ossService;
    }

    @Override
    public AliyunOssService createMultiOss(AliyunOssConfig config) {
        return new AliyunOssService(config, BeanFactory.getExecutor());
    }

    @Override
    public AliyunOssConfig getConfig() {
        return ConfigHolder.config;
    }

    @Override
    public AliyunOssService refresh(AliyunOssConfig config) {
        setConfig(config);
        ossService = createMultiOss(config);
        return ossService;
    }

    void setConfig(AliyunOssConfig config) {
        ConfigHolder.config = config;
    }

    public static AliyunOssFactory instance() {
        return INSTANCE;
    }
}
