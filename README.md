<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">oss4j</h1>
<h4 align="center" style="margin: 30px 0 30px; font-weight: bold;">oss4j 云对象存储接入工具</h4>

### 前言
整合各大云厂商对象存储，云存储接入仅需一行代码，支持：默认配置，动态刷新配置，方法级自定义配置，灵活方便。
工具统一了各厂商OSS接入标准，更换OSS厂商只需要更改yml配置文件即可。
如果项目对你产生了帮助，或者觉得还算值得鼓励，请点上一个star

### 支持厂商
- **阿里云OSS**
- **腾讯云COS**
- **华为云OBS**
- **七牛云**
- **又拍云**
- **私有云MINIO**

### SpringBoot集成
1. #### Maven引入
   ```xml
   <dependency>
       <groupId>com.gitee.tingen</groupId>
       <artifactId>oss4j-spring-boot-starter</artifactId>
       <version>1.1</version>
   </dependency>
   ```

2. #### 配置文件
   ```yaml
   oss:
      aliyun:
         access-key: xxx
         access-key-secret: xxxx
         bucket: b0
         end-point: http://oss-cn-shenzhen.aliyuncs.com
      minio:
         access-key: xxx
         access-key-secret: xxxx
         end-point: http://127.0.0.1:9000
         bucket: b0
      tencent:
         access-key: --
         access-key-secret: --
         bucket: --
         region: --
      qiniu:
         access-key: --
         access-key-secret: --
         bucket: --
         file-domain: --
      huawei:
         access-key: --
         access-key-secret: --
         bucket: --
         end-point: obs.cn-east-3.myhuaweicloud.com
         region: cn-east-3
         bucket-auto-create: true
   ```
   > 存储桶不存在时，支持通过配置（bucket-auto-create=true），默认创建存储桶

3. #### 示例
   ```java
      @RestController
      @RequestMapping("/upload/")
      public class UploadController {
         /**
          * 使用默认yml配置
          */
         @RequestMapping("/ali")
          public void ali() {
             File file = new File("1.pdf");
               //阿里云OSS，华为：OssSupplierType.HUAWEI
             BaseOssService ossService = OssFactory.createOssService(OssSupplierType.ALI);
             ossService.upload(file);
          }
          /**
           * 动态刷新配置
           */
          @RequestMapping("/huawei")
          public void huawei() {
              File file = new File("1.pdf");
              //使用默认配置创建一个OssService，然后使用refresh刷新配置，替换yml的默认配置
              BaseOssService ossService = OssFactory.createOssService(OssSupplierType.HUAWEI);
              ossService.refresh(QiniuOssConfig.builder().accessKey("xxx").build());
              ossService.upload(file);
          }
         /**
          * 自定义配置
          */
         @RequestMapping("/qiniu")
         public void qiniu() {
             File file = new File("1.pdf");
            //七牛OSS，自定义配置
            QiniuOssConfig config = QiniuOssConfig.builder().accessKey("xxx").build();
            BaseOssService ossService = OssFactory.createOssService(OssSupplierType.QINIU,config);
            ossService.upload(file);
         }
          /**
           * 自定义文件存储key
           */
          @RequestMapping("/qiniu")
          public void qiniu() {
              File file = new File("1.pdf");
              //七牛OSS，自定义配置
              QiniuOssConfig config = QiniuOssConfig.builder().accessKey("xxx").build();
              BaseOssService ossService = OssFactory.createOssService(OssSupplierType.QINIU,config);
              // ossService.upload(file) 是内部实现了一个文件key的实现
              ossService.upload(file,x->x.getName);
          }
      }
   ```
   > 切换OSS厂商只需要修改OssSupplierType枚举值

### JFinal集成
1. #### Maven引入
   ```xml
   <dependency>
      <groupId>com.gitee.tingen</groupId>
      <artifactId>oss4j-jfinal-plugin</artifactId>
      <version>1.1</version>
   </dependency>
   ```

2. #### 配置文件
   ```properties
   oss.aliyun.access-key=
   oss.aliyun.access-key-secret=
   oss.aliyun.bucket=
   oss.aliyun.end-point=
   ```

3. #### 示例
   ```java
      Oss4jPlugin plugin = new Oss4jPlugin("jboot.properties");
      plugins.add(plugin);
   ```

### Solon集成
1. #### Maven引入
   ```xml
   
   ```

2. #### 配置文件
   ```properties
   
   ```

3. #### 示例
   ```java
      
   ```
   
### 计划
- **oss4j-JFinal-plugin插件开发**
- **oss4j-solon-plugin插件开发**
