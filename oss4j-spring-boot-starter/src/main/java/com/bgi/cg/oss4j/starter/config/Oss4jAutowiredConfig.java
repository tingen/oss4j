package com.bgi.cg.oss4j.starter.config;

import com.bgi.cg.oss4j.common.config.OssConfig;
import com.bgi.cg.oss4j.common.factory.BeanFactory;
import com.bgi.cg.oss4j.core.factory.OssSupplierFactory;
import com.bgi.cg.oss4j.provider.aliyun.config.AliyunOssConfig;
import com.bgi.cg.oss4j.provider.huawei.config.HuaweiOssConfig;
import com.bgi.cg.oss4j.provider.minio.config.MinioOssConfig;
import com.bgi.cg.oss4j.provider.qiniu.config.QiniuOssConfig;
import com.bgi.cg.oss4j.provider.tencent.config.TencentOssConfig;
import com.bgi.cg.oss4j.provider.upyun.config.UpyunOssConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.Executor;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Slf4j
public class Oss4jAutowiredConfig {

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "oss")     //指定配置文件注入属性前缀
    protected OssConfig ossConfig() {
        return BeanFactory.getOssConfig();
    }

    @Bean("ossExecutor")
    protected Executor taskExecutor(OssConfig config) {
        return BeanFactory.setExecutor(config);
    }

    // Supplier Config
    @Bean
    @ConfigurationProperties(prefix = "oss.aliyun")
    protected AliyunOssConfig aliyunOssConfig() {
        return OssSupplierFactory.getAliyunConfig();
    }

    @Bean
    @ConfigurationProperties(prefix = "oss.huawei")
    protected HuaweiOssConfig huaweiOssConfig() {
        return OssSupplierFactory.getHuaweConig();
    }

    @Bean
    @ConfigurationProperties(prefix = "oss.minio")
    protected MinioOssConfig minioOssConfig() {
        return OssSupplierFactory.getMinioConfig();
    }

    @Bean
    @ConfigurationProperties(prefix = "oss.tencent")
    protected TencentOssConfig tencentOssConfig() {
        return OssSupplierFactory.getTencentConfig();
    }

    @Bean
    @ConfigurationProperties(prefix = "oss.qiniu")
    protected QiniuOssConfig QiniuOssConfig() {
        return OssSupplierFactory.getQiniuConfig();
    }
    @Bean
    @ConfigurationProperties(prefix = "oss.upyun")
    protected UpyunOssConfig UpyunOssConfig() {
        return OssSupplierFactory.getUpyunConfig();
    }
}
