package com.bgi.cg.oss4j.common.util;

import cn.hutool.core.util.StrUtil;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/10/7
 * @Version 1.0
 **/
public class StringUtil {
    public static String toCamelCase(CharSequence name, char symbol, boolean otherCharToLower) {
        if (null == name) {
            return null;
        }

        final String name2 = name.toString();
        if (StrUtil.contains(name2, symbol)) {
            final int length = name2.length();
            final StringBuilder sb = new StringBuilder(length);
            boolean upperCase = false;
            for (int i = 0; i < length; i++) {
                char c = name2.charAt(i);

                if (c == symbol) {
                    upperCase = true;
                } else if (upperCase) {
                    sb.append(Character.toUpperCase(c));
                    upperCase = false;
                } else {
                    sb.append(otherCharToLower ? Character.toLowerCase(c) : c);
                }
            }
            return sb.toString();
        } else {
            return name2;
        }
    }
}
