package com.bgi.cg.oss4j.common.config;

import lombok.Data;

/**
 * @Description TODO
 * @Author Z.cg
 * @Date 2023/9/8
 * @Version 1.0
 **/
@Data
public class OssConfig {
    /**
     * 核心线程池大小
     */
    private Integer corePoolSize = 10;

    /**
     * 最大线程数
     */
    private Integer maxPoolSize = 30;

    /**
     * 队列容量
     */
    private Integer queueCapacity = 50;

    private Integer downloadUrlExpireMs = 60;
}
