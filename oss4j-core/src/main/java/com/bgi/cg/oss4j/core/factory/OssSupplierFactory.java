package com.bgi.cg.oss4j.core.factory;

import com.bgi.cg.oss4j.common.config.OssConfig;
import com.bgi.cg.oss4j.provider.aliyun.config.AliyunOssConfig;
import com.bgi.cg.oss4j.provider.aliyun.factory.AliyunOssFactory;
import com.bgi.cg.oss4j.provider.huawei.config.HuaweiOssConfig;
import com.bgi.cg.oss4j.provider.huawei.factory.HuaweiOssFactory;
import com.bgi.cg.oss4j.provider.minio.config.MinioOssConfig;
import com.bgi.cg.oss4j.provider.minio.factory.MinioOssFactory;
import com.bgi.cg.oss4j.provider.qiniu.config.QiniuOssConfig;
import com.bgi.cg.oss4j.provider.qiniu.factory.QiniuOssFactory;
import com.bgi.cg.oss4j.provider.tencent.config.TencentOssConfig;
import com.bgi.cg.oss4j.provider.tencent.factory.TencentOssFactory;
import com.bgi.cg.oss4j.provider.upyun.config.UpyunOssConfig;
import com.bgi.cg.oss4j.provider.upyun.factory.UpyunOssFactory;

/**
 * @author  Z.cg
 **/
public class OssSupplierFactory {
    private OssSupplierFactory() {
    }

    public static AliyunOssConfig getAliyunConfig() {
        return AliyunOssFactory.instance().getConfig();
    }

    public static HuaweiOssConfig getHuaweConig() {
        return HuaweiOssFactory.instance().getConfig();
    }

    public static MinioOssConfig getMinioConfig() {
        return MinioOssFactory.instance().getConfig();
    }

    public static TencentOssConfig getTencentConfig() {
        return TencentOssFactory.instance().getConfig();
    }

    public static QiniuOssConfig getQiniuConfig() {
        return QiniuOssFactory.instance().getConfig();
    }
    public static UpyunOssConfig getUpyunConfig() {
        return UpyunOssFactory.instance().getConfig();
    }
}
