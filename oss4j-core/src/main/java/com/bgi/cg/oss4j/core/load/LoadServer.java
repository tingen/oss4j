package com.bgi.cg.oss4j.core.load;

import com.bgi.cg.api.BaseOssService;

/**
 * @author Z.cg
 */
public class LoadServer {
    private BaseOssService ossService;
    private int weight;
    private int currentWeight;

    protected LoadServer(BaseOssService ossService, int weight, int currentWeight) {
        this.ossService = ossService;
        this.weight = weight;
        this.currentWeight = currentWeight;
    }

    protected BaseOssService getOssServer() {
        return ossService;
    }

    protected int getWeight() {
        return weight;
    }

    protected int getCurrentWeight() {
        return currentWeight;
    }

    /**
     * 设置 BaseOssService
     */
    protected void setSmsServer(BaseOssService ossService) {
        this.ossService = ossService;
    }

    /**
     * 设置 weight
     */
    protected void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * 设置 currentWeight
     */
    protected void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }
}
