package com.bgi.cg.oss4j.core.factory;

import com.bgi.cg.api.config.OssSupplierConfig;
import com.bgi.cg.oss4j.provider._base.BaseOssProviderFactory;
import com.bgi.cg.oss4j.provider._base.OssSupplierType;
import com.bgi.cg.api.BaseOssService;

import java.util.function.Supplier;

/**
 * @author  Z.cg
 **/
public abstract class OssFactory {
    private OssFactory() {
    }

    public static BaseOssService createOssService(OssSupplierType ossSupplierType) {
        BaseOssProviderFactory baseOssProviderFactory = ossSupplierType.getOssProviderFactory();
        return baseOssProviderFactory.createOss();
    }

    public static BaseOssService createOssService(OssSupplierType ossSupplierType, OssSupplierConfig supplierConfig) {
        BaseOssProviderFactory baseOssProviderFactory = ossSupplierType.getOssProviderFactory();
        return baseOssProviderFactory.createMultiOss(supplierConfig);
    }
}
